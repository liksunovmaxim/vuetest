export default {
  GET_DATA(state, payload) {
    state.data = payload.data;
  },
  TOGGLE_LOADER(state, payload) {
    state.fetching = payload;
  },
};
