export default {
  fetchData(context, payload) {
    context.commit('GET_DATA', payload);
  },
};
